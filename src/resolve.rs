use http::uri::Uri;
use http::{Method, Request, Response};
use std::convert::Infallible;

/// Defines the matching behaviour the resolver should use with an instance that implements the Resolve trait
pub enum MatchingBehaviour {
    /// Partial Match : The paths should match up to the end of the Resolve's path. Intended for Routers.
    PARTIAL,
    /// Full Match : The paths should have the same number of sub-elements and those sub-elements should match together. Intended for Routes
    FULL,
}

/// A trait representing the ability to be resolved by an Esper Router.
///
/// *To be resolved means to get a response from a given Http Method and Path*
pub trait Resolve<T> {
    fn method(&self) -> &Method;
    fn uri(&self) -> &Uri;
    fn matching_behaviour(&self) -> &MatchingBehaviour;

    fn resolve(&self, req: Request<T>) -> Result<Response<T>, Infallible>;
}
