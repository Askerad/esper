use super::resolve::{MatchingBehaviour, Resolve};
use http::uri::Uri;
use http::{Method, Request, Response, StatusCode};
use std::collections::HashMap;
use std::convert::Infallible;

pub type RouteMethod<T> = dyn Fn(Request<T>) -> Result<Response<T>, Infallible>;

pub struct RouteParameters {
    parameters: HashMap<String, String>,
}

impl RouteParameters {
    pub fn get(&self, name: &str) -> Option<&String> {
        self.parameters.get(name)
    }

    pub fn from(map: HashMap<String, String>) -> RouteParameters {
        RouteParameters { parameters: map }
    }
}

pub struct Route<T> {
    uri: Uri,
    method: Method,
    function: Box<RouteMethod<T>>,
}

impl<T: Default> Default for Route<T> {
    fn default() -> Route<T> {
        Route {
            uri: Uri::default(),
            method: Method::default(),
            function: Box::new(|_req| {
                let response = Response::builder()
                    .status(StatusCode::OK)
                    .body(T::default())
                    .unwrap();

                Ok(response)
            }),
        }
    }
}
impl<T: Default> Route<T> {
    pub fn builder() -> RouteBuilder<T> {
        RouteBuilder::new()
    }
}

impl<T> Resolve<T> for Route<T> {
    fn method(&self) -> &Method {
        &self.method
    }
    fn uri(&self) -> &Uri {
        &self.uri
    }
    fn matching_behaviour(&self) -> &MatchingBehaviour {
        &MatchingBehaviour::FULL
    }
    fn resolve(&self, req: Request<T>) -> Result<Response<T>, Infallible> {
        (self.function)(req)
    }
}

pub struct RouteBuilder<T> {
    uri: Uri,
    method: Method,
    function: Box<RouteMethod<T>>,
}

impl<T: Default> RouteBuilder<T> {
    pub fn new() -> RouteBuilder<T> {
        let fun = |_req: Request<T>| -> Result<Response<T>, Infallible> {
            let body: T = T::default();
            let response = Response::builder()
                .status(StatusCode::BAD_REQUEST)
                .body(body)
                .unwrap();

            Ok(response)
        };

        RouteBuilder {
            uri: Uri::default(),
            method: Method::GET,
            function: Box::new(fun),
        }
    }

    pub fn method(mut self, method: Method) -> RouteBuilder<T> {
        self.method = method;
        self
    }

    pub fn uri(mut self, uri: Uri) -> RouteBuilder<T> {
        self.uri = uri;
        self
    }

    pub fn function(mut self, function: Box<RouteMethod<T>>) -> RouteBuilder<T> {
        self.function = function;
        self
    }

    pub fn spawn(self) -> Route<T> {
        Route {
            method: self.method,
            uri: self.uri,
            function: self.function,
        }
    }
}
