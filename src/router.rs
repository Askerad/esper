use super::resolve::{MatchingBehaviour, Resolve};
use super::route::{Route, RouteMethod, RouteParameters};
use http::uri::Uri;
use http::{Method, Request, Response};
use std::collections::HashMap;
use std::convert::Infallible;

pub struct Router<T> {
    uri: Uri,
    method: Method,
    routes: Vec<Box<dyn Resolve<T>>>,
}

impl<T: 'static + Default> Resolve<T> for Router<T> {
    fn method(&self) -> &Method {
        &self.method
    }
    fn uri(&self) -> &Uri {
        &self.uri
    }
    fn matching_behaviour(&self) -> &MatchingBehaviour {
        &MatchingBehaviour::PARTIAL
    }

    fn resolve(&self, mut req: Request<T>) -> Result<Response<T>, Infallible> {
        for route in &self.routes {
            if Router::match_route(&req, &route) {
                let params = Router::extract_params(&req, &route);
                req.extensions_mut().insert(params);

                let new_uri = cut_up_uri(req.uri(), route.uri());
                *req.uri_mut() = new_uri;
                return route.resolve(req);
            }
        }
        Ok(Response::builder().body(T::default()).unwrap())
    }
}

fn cut_up_uri(uri: &Uri, route_uri: &Uri) -> Uri {
    let route_path_items: Vec<&str> = route_uri.path().split('/').collect();
    let route_path_item_count = route_path_items.len();
    let mut request_path_items: Vec<&str> = uri.path().split('/').collect();
    request_path_items.drain(0..route_path_item_count);
    (String::from("/") + &request_path_items.join("/"))
        .parse::<Uri>()
        .unwrap()
}

impl<T: 'static + Default> Router<T> {
    pub fn set_uri(&mut self, uri: Uri) -> &mut Router<T> {
        self.uri = uri;
        self
    }

    pub fn add(&mut self, route: Box<dyn Resolve<T>>) -> &mut Router<T> {
        self.routes.push(route);

        self
    }
    pub fn get(&mut self, path: &str, function: Box<RouteMethod<T>>) -> &mut Router<T> {
        let route = Route::builder()
            .uri(path.parse().unwrap())
            .method(Method::GET)
            .function(function)
            .spawn();
        self.add(Box::new(route))
    }

    pub fn post(&mut self, path: &str, function: Box<RouteMethod<T>>) -> &mut Router<T> {
        let route = Route::builder()
            .uri(path.parse().unwrap())
            .method(Method::POST)
            .function(function)
            .spawn();
        self.add(Box::new(route))
    }

    pub fn delete(&mut self, path: &str, function: Box<RouteMethod<T>>) -> &mut Router<T> {
        let route = Route::builder()
            .uri(path.parse().unwrap())
            .method(Method::DELETE)
            .function(function)
            .spawn();
        self.add(Box::new(route))
    }

    pub fn put(&mut self, path: &str, function: Box<RouteMethod<T>>) -> &mut Router<T> {
        let route = Route::builder()
            .uri(path.parse().unwrap())
            .method(Method::PUT)
            .function(function)
            .spawn();
        self.add(Box::new(route))
    }

    pub fn patch(&mut self, path: &str, function: Box<RouteMethod<T>>) -> &mut Router<T> {
        let route = Route::builder()
            .uri(path.parse().unwrap())
            .method(Method::PATCH)
            .function(function)
            .spawn();
        self.add(Box::new(route))
    }

    fn extract_params(req: &Request<T>, route: &Box<dyn Resolve<T>>) -> RouteParameters {
        let path_items: Vec<&str> = req.uri().path().split("/").collect();
        let route_path_items: Vec<&str> = route.uri().path().split("/").collect();

        let mut ret: HashMap<String, String> = HashMap::new();

        for (index, _item) in route_path_items.iter().enumerate() {
            if route_path_items[index].len() > 0
                && route_path_items[index].chars().next().unwrap() == ':'
            {
                let value = path_items[index];
                let mut matching_name = route_path_items[index].chars();
                matching_name.next(); //yeeting the first character
                let key = String::from(matching_name.collect::<String>());
                ret.insert(key, String::from(value));
            }
        }

        RouteParameters::from(ret)
    }

    fn match_route(req: &Request<T>, route: &Box<dyn Resolve<T>>) -> bool {
        let path_items: Vec<&str> = req.uri().path().split("/").collect();
        let route_path_items: Vec<&str> = route.uri().path().split("/").collect();

        match route.matching_behaviour() {
            MatchingBehaviour::FULL => {
                if path_items.len() != route_path_items.len() {
                    return false;
                }

                let mut ret: bool = true;

                for (index, item) in path_items.iter().enumerate() {
                    if !RouterUtils::compare_path_elements(&item, &route_path_items[index]) {
                        ret = false;
                    }
                }
                ret
            }
            MatchingBehaviour::PARTIAL => {
                if path_items.len() < route_path_items.len() {
                    return false;
                }

                let mut ret: bool = true;

                for (index, item) in route_path_items.iter().enumerate() {
                    if !RouterUtils::compare_path_elements(&item, &path_items[index]) {
                        ret = false;
                    }
                }
                ret
            }
        }

        // if req.method() == (*route).method() && req.uri().path() == (*route).path() {
        //     true
        // } else {
        //     false
        // }
    }
}

impl<T> Default for Router<T> {
    fn default() -> Router<T> {
        Router {
            uri: Uri::default(),
            method: Method::default(),
            routes: Vec::default(),
        }
    }
}

struct RouterUtils;

impl RouterUtils {
    fn compare_path_elements(s1: &str, s2: &str) -> bool {
        if s1.len() > 0 && s2.len() > 0 {
            if s1.chars().next().unwrap() == ':' {
                return true;
            }
            if s2.chars().next().unwrap() == ':' {
                return true;
            }
        }

        if s1 == s2 {
            return true;
        }
        false
    }
}
